# aves-template

Aves template directory

Used with (aves)[https://www.npmjs.com/package/aves]

## Philosophy 

Make redundant file generation easier using the templating power of handlebars.
You just need to create template files and aves will generate files for you using cmd line.

## Usage

Install aves using the following command

```sh
sudo npm i -g aves
```
Then clone this repo inside your working directory

```sh
git clone https://bitbucket.org/utkarshsharma/aves-template templates
```

## Using the templates

```sh
aves -t hello --name earth
```

You can create new templates by appending the config.js file and adding templateFiles under the folder templateFiles

* aves command takes a template flag name with flag -t or --template 
* rest of the flags passed converted to variables of the flagname


