module.exports={
	hello:{ // name of the template
		files:[ // all the files it will generate
 			{
				name:'{{name}}.world',
				templatePath:'./templateFiles/hello.txt',
				destinationPath:'./'
			},
			{
				name:'{{name}}.java',
				templatePath:'./templateFiles/hello.txt',
				destinationPath:'./'
			},

		],

		transformer: function(obj) {
			if(obj.hasOwnProperty('name'))
				obj.name= obj.name.toUpperCase();
			return obj;
		}
	}
}